    extern printf

    section .data

fmtf: db "%f",10,0
fmte: db "%e",10,0
flt: dq 0.375       ; A floating point number assigned to a quadword

    section .text

    global main

main:

    push rbp

    mov rdi, fmtf
    movq xmm0, qword [flt]
    mov rax, 1
    call printf

    mov rdi, fmte
    movq xmm0, qword [flt]
    mov rax, 1
    call printf

    pop rbp

    mov rax, 0
    ret
    
