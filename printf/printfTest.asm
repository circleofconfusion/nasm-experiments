    extern printf

section .data

    str: db 'H','e','l','l','o',' ','w','o','r','l','d','!','!','!',0  
                                ; str somehow needs to be null-terminated
                                ; before being fed to printf.
                                ; May as well do it here.
    str1: db "Hello, world again!",0
    fmt: db "%s",10             ; Put the 10 there so that you get
                                ; the desired line break at the end.

section .bss

    conc resq 2

section .text
    
    global main

main:

    push rbp

    mov rdi, fmt
    mov rsi, str
    call printf

    mov rdi, fmt
    mov rsi, str1
    call printf

    pop rbp

    mov rax, 0
    ret
