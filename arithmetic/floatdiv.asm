    extern printf

    section .data
fmt1:       db "%f",10,0
flt1:       dq 0.375
dividend1:  dq 10.0

    section .text

    global main

main:

    push rbp

    mov rdi, fmt1
    movq xmm0, qword [flt1]
    mov rax,1
    call printf

    fld     qword   [flt1]
    fdiv    qword   [dividend1]
    fstp    qword   [flt1]

    mov     rdi, fmt1
    movq    xmm0, qword [flt1]
    mov rax, 1
    call printf
    

    pop rbp

    mov rax,0
    ret
