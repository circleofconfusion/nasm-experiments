    extern printf

    section .data
fmt:       db "%f",10,0
divisor:   dq 100
dividend:  dq 3

    section .text

    global main

main:

    push rbp

    fild     qword  [dividend]      ; Load an integer as a floating point 
                                    ; number onto the stack.
    fidiv    dword [divisor]        ; Divide an integer into the loaded float 
                                    ; and put the float result on the stack.
                                    ; For some reason, qword doesn't work here.
                                    ; Use a 32 bit divisor instead. Whateva.
    fstp    qword   [dividend]      ; Pop the resulting float back into 
                                    ; [dividend] 

    mov     rdi, fmt
    movq    xmm0, qword [dividend]  ; Use a floating point register 
                                    ; instead of rsi.
    mov rax, 1                      ; Tell printf to look in xmm0 instead 
                                    ; of rsi. 
    call printf
    

    pop rbp

    mov rax,0
    ret
