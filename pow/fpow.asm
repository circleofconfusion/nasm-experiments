    extern printf

    section .data

b:   dd 1.5
fmt: db "%e", 10, 0

    section .text
    global main
main:

    fld dword [b]
    mov rcx, 3
    call pow
    fstp dword [b]

    mov rax, 0
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; raises $st0 to the power of rcx                                             ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    section .data
base:   dd 0

    section .text
pow:
    fst dword [base]    ; put a copy of st0 in memory
    sub rcx, 1          ; subtract 1 from rcx to make the loop count accurate

.loop:
    fld dword [base]    ; push a copy of st0 on the stack
    fmul        ; multiply st1 and st1 storing the result in st0
    loop .loop  ; do this rcx number of times 

    .return:
    ret 
