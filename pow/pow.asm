    global main

    extern printf

%macro pow 2            ; Define a new macro 'pow' that takes 2 arguments

    section .text
    
    mov rax, %1 
    mov rbx, rax
    mov rcx, %2-1       ; counting down to 0, so subtract 1 from the power

powloop:

    imul rax, rbx
    loop powloop

%endmacro

    section .data

fmt: db "%d", 10, 0

    section .text

main:
    push rbp

    push rax
    push rbx
    push rcx

    pow 10, 3

br:
    mov rdi, fmt
    mov rsi, rax
    call printf

    pop rcx
    pop rbx
    pop rax

    pop rbp
    
    mov rax, 0
    ret
