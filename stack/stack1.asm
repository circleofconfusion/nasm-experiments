    extern printf

    section .data

dec1: db '1','2','3','4','5',0
fmt:  db "%ld",10,0

    section .text

    global main

main:

    push rbp

    mov rax,0               ; It seems that you need to initialize rax.
    mov rcx,0               ; Set rcx to 0 since it's our offest of bytes
                            ; from whatever dec1 is pointing to.

loop1:

    mov al, [dec1+rcx]      ; Copy the character found at dec1 + rcx number
                            ; of bytes into the bottom of rax.

    cmp rax, 0              ; Compare rax to the null character.

    je loop2                ; If we've gotten to the null char, 
                            ; jump to printing.

    sub rax, 48             ; Subtract 48 from the char number 
                            ; to get its actual decimal number.

    push rax                ; Push the ENTIRE CONTENTS (64 bits) 
                            ; of rax onto the stack. 

    inc rcx                 ; Increment r cx.

    jmp loop1               ; Keep on jumping until the cmp rax,0
                            ; instruction comes out equal.

loop2:

    mov rdi, fmt            ; Copy the string formatter into 
                            ; the destination index.

    pop rsi                 ; Suck 64 bits of previously stored information,
                            ; enough to fill up rsi, off the top of the stack.
                            ; This info was originally pushed from rax in loop1

    mov rax, 0              ; No floating points going on here.

    push rcx                ; Push rcx to the stack so printf doesn't clobber
                            ; it. Note that we're just pushing 64 bits of info.

    call printf             ; Run the printf C function.

    pop rcx                 ; Fill up rcx with bits by sucking the last 64
                            ; bits off the stack.
    
    loop loop2              ; The loop instruction will decrement rcx, 
                            ; and when it reaches 0, it will allow execution
                            ; to move on to the next line.

    pop rbp                 ; Pop the base pointer off the stack.

    mov rax, 0              ; Set the exit status.
    ret                     ; We're OUTTA here!
