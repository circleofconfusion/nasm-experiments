    extern printf

    section .data

bin1: dq  110011B
fmt:  db  "%ld", 10, 0

    section .text

    global main

main:

    push rbp

    mov rcx, 1

loop1:

    mov rbx, rcx
    and rbx, [bin1]
   
    push rbx
    push rcx

    mov rdi, fmt

    mov rsi, rbx

    mov rax, 0

    call printf

    pop rcx
    pop rbx

    imul rcx, 2

    cmp rcx, [bin1]

    jl loop1

finish:
    pop rbp
    mov rax, 0
    ret
