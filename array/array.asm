    extern printf

    section .bss
array:  resq    100

    section .data
fmt:    db  "%ld", 10, 0

    section .text

    global main

main:

    push rbp

    mov rcx, 0 
    mov rax, 100
    mov [array+rcx*8], rax
    inc rcx

    mov rax, 200
    mov [array+rcx*8], rax
    inc rcx

    mov rax, 300
    mov [array+rcx*8], rax

    mov rcx, 0

    mov rdi, fmt
    mov rsi, [array+rcx*8]
    mov rax, 0

    push rcx
    call printf
    pop rcx
    inc rcx

    mov rdi, fmt
    mov rsi, [array+8*rcx]
    mov rax,0
    push rcx
    call printf
    pop rcx
    inc rcx

    mov rdi, fmt
    mov rsi, [array+8*rcx]
    mov rax, 0
    push rcx
    call printf
    pop rcx

    pop rbp
    mov rax, 0
    ret
