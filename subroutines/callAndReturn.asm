    section .data
msg:    db  "Hello world!",0

    section .rodata
lf:     db  10

    section .text

    global main
main:
    push rbp

    call print
    call linefeed
    pop rbp
    mov rax, 0
    ret

print:              ; if subroutine is only called from main, 
                    ; there's no need for global declaration

    mov rdx, 12     ; set the length of the message referred to in rcx
    mov rcx, msg    ; move the memory address of the message to rcx
    mov rbx, 1      ; set the stdout file descriptor
    mov rax, 4      ; set the SYS_WRITE system call
    int 80h         ; tell the kernel it has an instruction
    ret             ; return to main

linefeed:
    mov rdx, 1      ; set the length of the linefeed
    mov rcx, lf     ; set the address of the linefeed character
    mov rax, 4      ; calling int 80h changes rax, so we need to reset it to 4 
    int 80h         ; once again interrupt the kernel
    ret             ; return to main
