    extern printf       ; Tell NASM to use the printf C function.

    section .data       ; Define a bunch of variables in memory.

dec1:       db  '1', '2', '3', '.', '4', '5', '6', 0
len:        equ $-dec1
fmt_char:   db  "%c", 10, 0 
fmt_dig:    db  "%ld", 10, 0

    section .text

    global main

main:
    push rbp            ; Push the base pointer onto the stack.

    mov rcx, 0          ; Set a counter to keep track of how many
                        ; characters we've printed.

part1:

loop1:                  ; Set the loop1 label that we can later jump to.

    push rcx            ; Push rcx onto the stack to protect it from printf.

    mov rdi, fmt_char   ; Copy the fmt_char string into the destination index.
    
    mov al, [dec1+rcx]  ; Copy the _value_ of the character
                        ; at the head of dec1 into the lowest part of 
                        ; the A register.
                        ; We have to use al, because we're only copying a
                        ; singly byte rather than a quad word.
                        ; rcx here represents the number of _bytes_ offset
                        ; from the beginning of dec1

    mov rsi, rax        ; Copy the entire A register into the source index.
                        ; Since we're using _r_si, which is 64 bit, 
                        ; we need to copy a 64 bit word into it, 
                        ; even though only the bottom byte of the 
                        ; A register is filled.

    mov rax, 0          ; Specify that we're not using any floating point
                        ; numbers in this printf function call.
     
    call printf         ; Print 'er out. The printf function looks for 
                        ; its first argument in rdi, and its
                        ; second argument in rsi
    
    pop rcx             ; Pull rcx off the stack now that printf has executed.

    inc rcx             ; Increment rcx.
    
    cmp rcx, len-1      ; Compare the now-incremented rcx to the length
                        ; of the character string - 1. 
                        ; We use len-1 because the last character is
                        ; the null terminator, and we don't want to print it.
    
    jne loop1           ; If the comparison didn't set the equal flag,
                        ; jump to the loop1 label.

part2:

    mov rcx, 0          ; Set the counter back to 0 for the next loop.

loop2:

    push rcx            ; Push rcx onto the stack to protect it from printf.

    mov rdi, fmt_dig    ; Copy fmt_dig into the destination index

    mov al, [dec1+rcx]  ; Copy the next character into al

    sub rax, 48         ; Subtract 48 from the integer 
                        ; representing the character.

    mov rsi, rax        ; Copy the value of rax into the source index.

    mov rax, 0          ; We are using no floating point values.

    call printf         ; Execute printf.

    pop rcx             ; Pop rcx off the stack since we're done printing.

    inc rcx             ; Increment rcx.

    cmp rcx, len-1      ; Compare rcx to the the length of the decimal 
                        ; string - 1.

    jne loop2           ; If the above comparison didn't set the equality flag,
                        ; jump back to loop 2.

    pop rbp             ; Clear out the stack.

    mov rax, 0          ; Set the return value to 0.
    ret                 ; End execution.
